//
//  TableViewController.swift
//  github-api-ios
//
//  Created by Innoventes Technology on 08/01/17.
//  Copyright © 2017 Chibde. All rights reserved.
//

import UIKit

class RepoTableViewController: UITableViewController,URLSessionDataDelegate,        URLSessionDelegate {
    var userName:String!
    var items = [Repos]()
    var buffer:NSMutableData = NSMutableData()

    override func viewDidLoad() {
        super.viewDidLoad()
        let session = URLSession.shared
        let url = URL(string: Constant.getRepoURI(user: "GautamChibde"))!
        let request = URLRequest(url: url)
        let task = session.dataTask(with: request) {
            (data, response, error) in
            guard let data = data else {
                print("No data was returned by the request!")
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [[String:AnyObject]]
                self.items = Model.parseRepo(repoList: json)
                self.tableView.reloadData()
            } catch {
                print("Could not parse the data as JSON: '\(data)'")
                return
            }
        }
        task.resume()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        buffer.append(data)
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("in table view")
        print(indexPath.row)
        let cell = Bundle.main.loadNibNamed("itemRepos", owner: self, options: nil)?.first as! itemRepos
        
        cell.title.text = items[indexPath.row].name
        return cell
    }
}
