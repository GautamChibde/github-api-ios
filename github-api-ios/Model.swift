//
//  Model.swift
//  github-api-ios
//
//  Created by Innoventes Technology on 08/01/17.
//  Copyright © 2017 Chibde. All rights reserved.
//

import UIKit

struct Repos {
    let id:Int!
    let name:String!
    let owner:User!
    let description:String!
    let stargazers_count:Int!
    let isForked:Bool!
    let isPrivate:Bool!
}

struct User {
    let id:Int!
    let login:String!
    let name:String
    //let date:Date!
    //let message:String!
    let email:String!
    let profileImage:String!
}

class Model{
    
    static func parseRepo(repoList : [[String:AnyObject]]) -> [Repos] {
        var itemList = [Repos]()
        for item in repoList {
            itemList.append(Repos(id : item["id"] as! Int,
                              name : item["name"] as! String,
                              owner : parseUser(user : item["owner"] as! [String:AnyObject]),
                              description : item["description"] as? String ?? "" ,
                              stargazers_count : item["stargazers_count"] as! Int,
                              isForked : item["fork"] as! Bool,
                              isPrivate : item["private"] as! Bool))
        }
        return itemList
    }
    
   static func parseUser(user : [String:AnyObject]) -> User {
        let u = User(id : user["id"] as! Int,
                     login: user["login"] as! String,
                     name: user["name"]  as? String ?? "",
                     email: user["email"] as? String ?? "" ,
                     profileImage : user["avatar_url"] as! String)
        return u
    }
}
