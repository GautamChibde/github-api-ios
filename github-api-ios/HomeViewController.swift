//
//  ViewController.swift
//  github-api-ios
//
//  Created by Innoventes Technology on 07/01/17.
//  Copyright © 2017 Chibde. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet var userName: UITextView!
    @IBAction func showRepos(_ sender: Any) {
        performSegue(withIdentifier: "showDetail", sender: userName.text)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier=="showDetail"{
            let playSoundVC = segue.destination as! DetailViewControlerViewController
            let userName = sender as! String
            playSoundVC.userName = userName
        }
    }
}

