//
//  DetailViewControlerViewController.swift
//  github-api-ios
//
//  Created by Innoventes Technology on 12/01/17.
//  Copyright © 2017 Chibde. All rights reserved.
//

import UIKit

class DetailViewControlerViewController: UIViewController {
    var userName:String!
    var user: User!

    @IBAction func showRepos(_ sender: Any) {
        performSegue(withIdentifier: "showRepos", sender: userName)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Gautam Chibde")
        let session = URLSession.shared
        let url = URL(string: Constant.getUserDetail(user: "GautamChibde"))!
        let request = URLRequest(url: url)
        let task = session.dataTask(with: request) {
            (data, response, error) in
            guard let data = data else {
                print("No data was returned by the request!")
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
                self.user = Model.parseUser(user: json)
                print(self.user)
                print("Gautam Chibde")
              
            } catch {
                print("Could not parse the data as JSON: '\(data)'")
                return
            }
        }
        task.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier=="showRepos"{
            let playSoundVC = segue.destination as! RepoTableViewController
            let userName = sender as! String
            playSoundVC.userName = userName
        }
    }
}
