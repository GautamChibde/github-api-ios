//
//  Constant.swift
//  github-api-ios
//
//  Created by Innoventes Technology on 08/01/17.
//  Copyright © 2017 Chibde. All rights reserved.
//

import UIKit

class Constant{

    static let BASE_URI = "https://api.github.com"
    
    static func getUserDetail(user : String) -> String{
        return Constant.BASE_URI + "/users/" + user
    }
    
    static func getRepoURI(user : String) -> String {
        return Constant.BASE_URI + "/users/" + user + "/repos"
    }
    
    static func getFollowing(user : String) -> String {
        return Constant.BASE_URI + "/users/" + user + "/following"
    }
  
    static func getFollowers(user : String) -> String{
        return Constant.BASE_URI + "/users/" + user + "/followers"
    }
    
    static func getCommits(user : String , repo : String) -> String{
        return Constant.BASE_URI + "/users/" + user + "/" + repo + "/commits"
    }
    
    static func getCollaborators(user : String , repo : String) -> String{
        return Constant.BASE_URI + "/users/" + user + "/" + repo + "/collaborators"
    }
}
